#!/usr/bin/python
"""
created by Kip
modified by Huanxin
"""

import os
from itertools import *
from math import *
from numpy import *

# -----------------------------------------------------------------
# Create a new directory, with a possible suffix for uniqueness.
# Return the name of the newly created directory.
#

def mknewdir(newdir):
    if os.path.exists(newdir):
        suffix = 1
        while os.path.exists(newdir+"-"+str(suffix)):
            suffix += 1
        newdir += "-"+str(suffix)
    os.mkdir(newdir)
    return newdir


# -----------------------------------------------------------------
# Simulation configuration
#


equilibsteps = int(5e5)
simsteps     = 1
dumpevery    = 1
num_dumps    = simsteps / dumpevery

sim_hours = 5000

dt = 0.3 # time step
Z = 3 # salt valency
R = 1 # globe radius
L = float(100) # linear system size 
ewald_accuracy = 1e-12
ewald_cutoff = L/2 # tunable parameter for performance
T = 1.2 # temperature
bjerrum = 3.0 # bjerrum length

sigma_mm = 1.0 # monomer diameter for LJ repulsion // 1.0
sigma_ii = 1.0 # ion diameter for LJ repulsion // 1.0
sigma_mi = (sigma_mm + sigma_ii) / 2.0

n_spheres = 2
n_patches = 372 # 372, 732, 1472, 3002
n_plate	  = 93 # 93, 183, 368, 750
#patch_aligned = False
patch_aligned = True
patch_mass = 1.0 / (n_patches + n_plate + 1) # mass equally distributed on patch and core
ion_x = 1.2 * R
print ("Distance between patches: %g" % sqrt(4./n_patches)*R)

dlc_accuracy = 1.e-4
e_bg  = 1 # background permittivity
e_obj = 400 # object permittivity

n_salt_molecules = 0
n_salt_counterions = n_salt_molecules
n_salt_coions = Z*n_salt_molecules

jobname = "janus.L%d" % (round(L))

dirname = mknewdir("./"+jobname) # mknewdir("/home/kbarros/scratch/dlc/"+jobname)
print "Created directory:\n " + dirname
lammps = "lmp_dlc"

confname = "in.dat"
dataname = "data.dat"
analyzername = "analyzer.scala"
dumpname = "dump.dat"
pbsname = "job.pbs"


# ------------------------------------------------------------------
# Geometry loader

class DielectricGeometry:
    def __init__(self, filename):
        self.cnt = 0
        self.pos = []
        self.norm = []
        self.area = []
        self.curv = []
        
        with open(filename) as file:
            for line in file.readlines():
                ls = line.strip()
                if len(ls) < 1 or ls[0] == '#':
                    continue
                [x,y,z,nx,ny,nz,area,curv] = map(float, ls.split())
                self.pos.append((x,y,z))
                self.norm.append((nx,ny,nz))
                self.area.append(area)
                self.curv.append(curv)
                self.cnt += 1

    # rotate object so that point (x0,y0,z0) gets mapped along vector (1, 0, 0)
    def rotate(self, (x0, y0, z0)):
        theta = atan2(y0, x0)                  # projected angle on x-y plane
        phi   = atan2(z0, sqrt(x0**2 + y0**2)) # angle out of x-y plane
        # precompute trigonometry
        ct = cos(theta)
        st = sin(theta)
        cp = cos(phi)
        sp = sin(phi)
        def rotateVector((x, y, z)):
            (x, y) = (x*ct + y*st, -x*st + y*ct) # rotate -theta degrees on z axis
            (x, z) = (x*cp + z*sp, -x*sp + z*cp) # rotate -phi degrees on y axis
            return (x, y, z)
        self.pos  = map(rotateVector, self.pos)
        self.norm = map(rotateVector, self.norm)

janus_sphere = DielectricGeometry("packings/geom_maxvol_{0}.txt".format(n_patches))
janus_plate = DielectricGeometry("packings/cci{0}_voronoi.txt".format(n_plate))

patch_align_x = {372:  (0.4469932118, 0.8449087706, 0.2938132705),
                 732:  (0.5966713550, 0.7983113863, 0.0817448760),
                 1472: (0.6205789838, 0.7794333412, 0.0858218588),
                 3002: (0.6205789838, 0.7794333412, 0.0858218588),}
patch_antialign_x = {372:  (-0.36180756900211575, 0.11752587136859942, 0.9248150910164856),
                     732:  (-0.6142312575491261, -0.38662274422961324, -0.6879264611090218),
                     1472: (0.759264354083356, 0.5093255495445894, 0.40509890792186304),}

if patch_aligned:
    janus_sphere.rotate(patch_align_x[n_patches]) # rotate patch 0 to x axis
else:
    janus_sphere.rotate(patch_antialign_x[n_patches]) # rotate center of (arbitrary) hole to x axis


# -----------------------------------------------------------------
# Generate LAMMPS config file
#

def generate_conf_file():
    return (
"""# automatically generated polymer configuration file

units		lj
boundary	p p p
dimension	3

atom_style	dielectric
neighbor	0.3 bin			# skin distance is 0.3 ("lennard jones" distance units)
neigh_modify	delay 5 page 500000 one 10000 # defaults: delay 10 page 100000 one 2000

read_data	%(dataname)s		# load volume dimensions, masses, atoms, and bonds
# read_restart	restart.equil.dat
restart		%(restart_freq)d restart/restart.*.dat
thermo		%(dumpevery)d		# steps between output of thermodynamic quantities 
thermo_modify	flush yes		# flush output in case of Lammps crash
thermo_modify	norm no			# don't normalize extensive quantities (e.g. energy)
timestep	%(dt)f			#

### DEFINE GROUPS
group		patch type 1
group		core type 2
group		spheres union patch core
group		nonspheres subtract all spheres

### SET INTEGRATION METHOD
# fix		1 nonspheres nve		# evolve all atoms at constant N,V,E (rigid bodies separate
# fix		2 spheres rigid molecule

#### SOFT DYNAMICS TO SPREAD ATOMS
# see gen_brush.py

### SWITCH TO COULOMB/LJ INTERACTIONS
#pair_style	lj/cut/coul/long 0 %(ewald_cutoff)f	# < LJ_cutoff coulomb_cutoff > (LJ_cutoff set below)
#pair_coeff	* *	0.01 %(sigma_mm)s %(cut_mm)s	# < type1 type2 epsilon sigma LJ_cutoff >
#pair_coeff	1*2 *	0.0 0.0 0.0			# disable LJ for sphere atoms

pair_style hybrid/overlay lj/cut 0.0 coul/long %(ewald_cutoff)f
pair_coeff * * lj/cut 0.00 %(sigma_mm)s %(cut_mm)s	# < type1 type2 epsilon sigma LJ_cutoff >
pair_coeff * * coul/long

pair_modify	shift yes		# LJ interactions shifted to zero

#set atom 1003 vx 1003
#set atom 1004 vx 1
#set atom 1005 vx 1

### KSPACE
kspace_style	pppm %(ewald_accuracy)g	# desired accuracy
# LAMMPS coulombic energy is (q1 q2) / (\eps r)
# bjerrum length is (\lambda_B = %(bjerrum)f)
# temperature is (k T = %(T)f)
# dielectric is \eps = (1 / k T \lambda_B)
# dielectric	%(dielectric)s
dielectric	12.56637

### DIELECTRIC SOLVER FIX
fix		3 all dielectric %(dlc_accuracy)e logForces logConvergence		# < accuracy >

### EQUILIBRATE WITH TEMPERATURE RESCALING
# see gen_brush.py

### PRODUCTION RUN WITH LANGEVIN DYNAMICS
dump		1 all custom %(dumpevery)d %(dumpname)s id type x y z ix iy iz vx vy vz q
#fix		7 all langevin %(T)s %(T)s 10.0 699483	# < Tstart Tstop damp seed [keyword values ... ] >
run		%(simsteps)d
#unfix		7
""" % {'dataname':dataname, 'dumpname':dumpname, 'simsteps':simsteps,
       'soft_equilibsteps':equilibsteps, 'equilibsteps':equilibsteps, 'dumpevery':dumpevery, 'restart_freq':20*dumpevery,
       'ewald_accuracy':ewald_accuracy, 'ewald_cutoff':ewald_cutoff, 'T':T, 'dt':dt, 'bjerrum':bjerrum,
       'dielectric':1.0/(T * bjerrum), 'dlc_accuracy':dlc_accuracy,
       'sigma_mm':sigma_mm, 'sigma_mi':sigma_mi, 'sigma_ii':sigma_ii,
       'cut_mm':sigma_mm*(2**(1./6)), 'cut_mi':sigma_mi*(2**(1./6)), 'cut_ii':sigma_ii*(2**(1./6))})


# -----------------------------------------------------------------
# Generate input data file
#

def generate_data_file():
    natoms = n_spheres*(n_patches + n_plate +1) + 2 # n_salt_counterions + n_salt_coions
    
    header = (
"""LAMMPS FENE chain data file

          %(natoms)d  atoms
          4  atom types

 %(lmin)f  %(lmax)f xlo xhi
 %(lmin)f  %(lmax)f ylo yhi
 %(lmin)f  %(lmax)f zlo zhi

""" % {'natoms':natoms, 'lmin':0, 'lmax':L})
    
    masses = (
"""Masses
  #
  1  %(patch_mass)e # surface charge patches
  2  %(patch_mass)e # core particles
  3  1.0 # salt counterions
  4  1.0 # salt coions

""" % {'patch_mass':patch_mass})
    
    def wrap_coordinate(x):
        i = 0
        while x >= L:
            x -= L
            i += 1
        while x < 0:
            x += L
            i -= 1
        return (x, i)
    
    def patch_str(atom_id, mol_id, q_surf, i, radius, offset):
        atom_type = 1
        e_in = e_obj
        e_out = e_bg
        area = janus_sphere.area[i]*radius**2
        charge = q_surf * area
        curvature = 1.0/radius
        nx,ny,nz = janus_sphere.norm[i]
        x_y_z = [px*radius+ox for (px,ox) in zip(janus_sphere.pos[i], offset)]
        (x,ix), (y,iy), (z,iz) = map(wrap_coordinate, x_y_z)
        return ("  %d  %d  %d  %.10f  %.10f  %.10f  %.10f  %.10f  %.10f  %.10f  %.10f  %.10f  %.10f  %.10f %d %d %d\n" %
                        (atom_id, mol_id, atom_type, charge, e_in, e_out, area, curvature, nx, ny, nz, x, y, z, ix, iy, iz))

    def plate_str(atom_id, mol_id, q_surf, i, radius, offset):
        atom_type = 1
        e_in = e_obj
        e_out = e_bg
        area = janus_plate.area[i]*radius**2
        charge = q_surf * area
        curvature = 0.0
        nx,ny,nz = janus_plate.norm[i]
        x_y_z = [px*radius+ox for (px,ox) in zip(janus_plate.pos[i], offset)]
        (x,ix), (y,iy), (z,iz) = map(wrap_coordinate, x_y_z)
        return ("  %d  %d  %d  %.10f  %.10f  %.10f  %.10f  %.10f  %.10f  %.10f  %.10f  %.10f  %.10f  %.10f %d %d %d\n" %
                        (atom_id, mol_id, atom_type, charge, e_in, e_out, area, curvature, nx, ny, nz, x, y, z, ix, iy, iz))
    
    def core_str(atom_id, mol_id, charge, x_y_z):
        atom_type = 2
        e_in = e_obj # atom resides in 'object' surrounded by 'background'
        e_out = e_bg
        area = 0
        curvature = 0
        nx, ny, nz = 0, 0, 0
        (x,ix), (y,iy), (z,iz) = map(wrap_coordinate, x_y_z)
        return ("  %d  %d  %d  %.10f  %.10f  %.10f  %.10f  %.10f  %.10f  %.10f  %.10f  %.10f  %.10f  %.10f %d %d %d\n" %
                        (atom_id, mol_id, atom_type, charge, e_in, e_out, area, curvature, nx, ny, nz, x, y, z, ix, iy, iz))
    
    def atom_str(atom_id, mol_id, atom_type, charge, x_y_z):
        e_in = e_bg # atom resides in 'background' solution
        e_out = e_bg
        area = 0
        curvature = 0
        nx, ny, nz = 0, 0, 0
        (x,ix), (y,iy), (z,iz) = map(wrap_coordinate, x_y_z)
        return ("  %d  %d  %d  %.10f  %.10f  %.10f  %.10f  %.10f  %.10f  %.10f  %.10f  %.10f  %.10f  %.10f 0 0 0\n" %
                        (atom_id, mol_id, atom_type, charge, e_in, e_out, area, curvature, nx, ny, nz, x, y, z))
    
    atoms = "Atoms\n  # id molecule type charge e_in e_out area curvature nx ny nz x y z ix iy iz\n"
    atom_id = 0
    mol_id = 0

#    offset = (R, R, R)
    offset = [[R, R, R], [3*R, 3*R, 3*R]]
    
    # create spheres
#    assert(n_spheres == 1)
    for sphere in range(n_spheres):
        mol_id += 1
        charge = 0
        # create patch atoms
        for patch in range(n_patches):
            atom_id += 1
            atoms += patch_str(atom_id, mol_id, charge, patch, R, offset[sphere])
	for plate in range(n_plate):
            atom_id += 1
            atoms += plate_str(atom_id, mol_id, charge, plate, R, offset[sphere])
        # create core atoms
        atom_id += 1
        atoms += core_str(atom_id, mol_id, charge, offset[sphere])

    # +1 charge near sphere
    atom_id += 1
    mol_id += 1
    atom_type = 4
    charge = +1
    x_y_z = (offset[0][0]+ion_x, offset[0][1], offset[0][2])
    atoms += atom_str(atom_id, mol_id, atom_type, charge, x_y_z)

    # -1 charge maximally away from sphere
    atom_id += 1
    mol_id += 1
    atom_type = 4
    charge = -1
    x_y_z = (L/2+offset[0][0], L/2+offset[0][1], L/2+offset[0][2])
    atoms += atom_str(atom_id, mol_id, atom_type, charge, x_y_z)
    
    atoms += "\n"
    
    return header + masses + atoms


# -----------------------------------------------------------------
# Generate scala file for data analysis
#

def generate_scala_analyzer_file():
    return (
""" // run using command "sc analyzer.scala"
import kip._
""")


# -----------------------------------------------------------------
# Generate PBS file
#

def generate_pbs_file():
    return (
"""
# ### AUTOMATICALLY GENERATED BATCH FILE

# ### name of job
#PBS -N %(jobname)s

# ### mail on end/abort
#PBS -m ea
#PBS -M kbarros@northwestern.edu

# ### maximum cpu time
#PBS -l cput=%(sim_hours)d:00:00

# ### number of nodes and processors per node
#PBS -l nodes=1:ppn=1

# ### indicates that job should not rerun if it fails
# #PBS -r n

# ### stdin and stderr merged as stderr
#PBS -j eo

# ### write stderr to file
#PBS -e %(dirname)s/log.err

# ### the shell that interprets the job script
#PBS -S /bin/bash

cd %(dirname)s 
%(lammps)s < %(confname)s &> lammps.out
ssh minotaur scp -r %(dirname)s achilles.ms.northwestern.edu:%(dirname)s-mino
if [ $? -eq 0 ] ; then
touch COMPLETED
fi
""" % {'jobname':jobname, 'sim_hours':sim_hours, 'dirname':dirname, 'lammps':lammps, 'confname':confname})



# -----------------------------------------------------------------
# Build a new simulation directory
#

def build_dir():
    root = dirname + "/"
    
    os.mkdir(root+"restart")    # make directory for restarts
    
    with open(root + confname, 'w') as f:
        f.write(generate_conf_file())
    
    with open(root + dataname, 'w') as f:
        f.write(generate_data_file())
    
    with open(root + analyzername, 'w') as f:
        f.write(generate_scala_analyzer_file())
    
    with open(root + pbsname, 'w') as f:
        f.write(generate_pbs_file())
    
    os.system('cp ' + os.path.realpath(__file__) + ' ' + root + 'generation.py')
    os.system('ln -fsn %s link##' % root)

build_dir()
