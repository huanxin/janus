#!/usr/bin/env python
import sys

def distance(x, y):
	x = float(x)
	y = float(y)
	print x**2 + y**2

name = 'cci' + str(sys.argv[1]) + '.txt'
area = 'area' + str(sys.argv[1]) + '.txt'
x_list = []
y_list = []
z = 0.0
area_list = []
nx = 0.0
ny = 0.0
nz = 1.0
curvature = 0.0
with open(name) as f:
	lines = f.readlines()
	for i in lines:
		[ID, x, y] = i.split()
		x_list.append(float(x))
		y_list.append(float(y))
		
with open(area) as f:
	lines = f.readlines()
	for i in lines:
		area = float(i.split()[0])
		area_list.append(area)

with open('cci' + str(sys.argv[1]) + '_voronoi.txt', 'w') as f:
	f.write("""# Grid for circular plate of radius 1 with optimal cicle packing. Areas of each patch are computed via Voronoi diagram
# {num_patch:s} patches
# x y z nx ny nz area curvature
""".format(num_patch = sys.argv[1]))
	assert len(x_list) == len(y_list) == len(area_list) 
#	assert len(y_list) == len(area_list)
	for i in range(len(x_list)):
		f.write('%15.10f%15.10f%15.10f%15.10f%15.10f%15.10f%15.10f%15.10f\n' % (x_list[i], y_list[i], z, nx, ny, nz, area_list[i], curvature))
	
